import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import Content from './component/content/content';
import Footer from './component/footer/footer';
import Header from './component/header/header';

function App() {
  return (
    <div>
      <Header></Header>
      <Content></Content>
      <Footer></Footer>
    </div>
  );
}

export default App;
