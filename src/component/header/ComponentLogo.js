import { Grid, Typography } from "@mui/material"

function ComponentLogo (){
    return(
    <Grid lg={6} item>
        <Typography style={{ fontSize: "35px", fontWeight: 900, paddingLeft: "200px" }}>Devcamp</Typography>
    </Grid>
    )
}

export default ComponentLogo