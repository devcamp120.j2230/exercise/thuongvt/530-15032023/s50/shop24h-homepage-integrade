import { Container } from "@mui/material"
import ContentCarousel from "./Carousel"
// import ComponentCarousel from "./ComponentCarousel"
import LastestProducts from "./LastestProducts"
import ViewAll from "./ViewAll"



function Content (){
    return(
      <Container>
        <ContentCarousel></ContentCarousel>
        {/* <ComponentCarousel></ComponentCarousel> */}
        <LastestProducts></LastestProducts>
        <ViewAll></ViewAll>
      </Container>
    )
}

export default Content