import { Grid, Typography } from "@mui/material"
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';

function ComponentIconNavBar() {
    return (
        <Grid lg={6}>
            <Typography style={{ paddingLeft: "100px", marginTop: "20px" }}>
                <NotificationsNoneIcon sx={{ cursor: "pointer", marginRight:"20px"}}></NotificationsNoneIcon>
                <AccountCircleIcon sx={{ cursor: "pointer",marginRight:"20px" }}></AccountCircleIcon>
                <AddShoppingCartIcon sx={{ cursor: "pointer",marginRight:"20px" }}></AddShoppingCartIcon>
            </Typography>
        </Grid>
    )
}

export default ComponentIconNavBar